package store;

public abstract class Component {
   private String manufacturer;
   private String partId;
   private String color;
   private double price;

   
public Component(String manufacturer, String partId, String color, double price) {
    this.manufacturer = manufacturer;
    this.partId = partId;
    this.color = color;
    this.price = price;
}
public String getManufacturer() {
    return manufacturer;
}
public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
}
public String getPartId() {
    return partId;
}
public void setPartId(String partId) {
    this.partId = partId;
}
public String getColor() {
    return color;
}
public void setColor(String color) {
    this.color = color;
}
public double getPrice() {
    return price;
}
public void setPrice(double price) {
    this.price = price;
}
}
