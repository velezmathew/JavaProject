package store;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ComponentTest{
    @Test
    public void testImplementation() {
        //create a test class that extends Component abstract class
        class testComponent extends Component{
            public testComponent(String manufacturer, String partId, String color, double price) {
                super(manufacturer, partId, color, price); 
            }
        }
        testComponent test = new testComponent("ASUS", "1hj32kjh", "red", 342);
        assertTrue(test.getManufacturer().equals("ASUS"));
        assertTrue(test.getPartId().equals("1hj32kjh"));
        assertTrue(test.getColor().equals("red"));
        assertTrue(test.getPrice() == 342);
    }

    
}
