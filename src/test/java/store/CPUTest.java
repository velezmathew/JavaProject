package store;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class CPUTest {
   @Test
   public void testConstructor() {
        CPU x = new CPU("Intel", "BX8071512700K", "green",234, "Intel i7-2600k", 12, "3.6Ghz","5Ghz",125,"LGA1700", "Intel Core i7");
        assertTrue(x.getManufacturer().equals("Intel"));
        assertTrue(x.getPartId().equals("BX8071512700K"));
        assertTrue(x.getColor().equals("green"));
        assertTrue(x.getPrice() == 234);
        assertTrue(x.getName().equals("Intel i7-2600k"));
        assertTrue(x.getCoreCount() == 12);
        assertTrue(x.getCoreClock().equals("3.6Ghz"));
        assertTrue(x.getBoostClock().equals("5Ghz"));
        assertTrue(x.getTdp() == 125);
        assertTrue(x.getSocket().equals("LGA1700"));
        assertTrue(x.getSeries().equals("Intel Core i7"));
      } 
}
